require 'sinatra'
require 'sinatra/json'
require 'json'

# PATCH /api/jobs/{job_id}
patch '/api/jobs/:job_id' do
  request.body.rewind
  data = JSON.parse request.body.read
  logger.info data
end

# GET /api/shipments/{shipper_reference}/label
get '/api/shipments/:shipper_reference/label' do
  content_type :json
  json({
    "shipper_reference": "#{params['shipper_reference']}",
    "carrier": "DHL_SMARTMAIL_EXPEDITED",
    "label_zpl": "^XA...^XZ",
    "tracking_number": "9274899990987300198319",
    "label_errors": ""
  })
end